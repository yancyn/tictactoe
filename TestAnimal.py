# python3.8 -m unittest TestAnimal.py
# see also https://realpython.com/python-testing/
from Animal import *
import unittest

class TestAnimal(unittest.TestCase):

    def test_init(self):
        target = Animal("Cat", "meow", 4)
        self.assertEqual("Cat", target.name, "Should be Cat")

    def test_says(self):
        target = Animal("Cat", "meow", 4)
        target.says()
        self.assertEqual("meow", target.sound, "Should be meow")
    
    def test_legs(self):
        target = Animal("Duck", "Quack", 2)
        self.assertEqual(2, target.numOfLegs, "Should be 2 legs")
