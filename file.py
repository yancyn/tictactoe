# how to read a file
file1 = open('test.txt')
print(file1.read())

# how to write a file
# w+ mean overwrite everytime
# a+ append and keep if exist
file2 = open('test2.txt', 'a+')
file2.write('This is a first line')
file2.write('\nThis is a second line')
