tupple1 = (1,2,3)
print(tupple1[0])
for t in tupple1:
    print(t)

# Failed
# tupple1[0] = 11
# print(tupple1[0])

index = tupple1.index(3)
print(index)

# Cannot be assign child of it but can be reassign whole variable
tupple1 = (2,3,4)
for t in tupple1:
    print(t)