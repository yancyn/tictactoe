print('Hello World 1')
print("Hello World 2")
print('مرحبا')
print('Use \n to print a new line')
print('\n')
print('See what I mean?')
print("I'm using single quote, but will create error")

stringA = 'Hello World'
print(stringA)

# we can reassign a string variable
stringA = 'Welcome'
print(stringA)
print(stringA[0])

# cannot reset on character value
# stringA[0] = 'A'
# print(stringA[0])

print(stringA[1:])
print(stringA[0:5])
print(stringA[:5])
print(stringA[-1])
print()
for s in stringA:
    print(s)

stringA += " Back"
print(stringA)
arrayA = stringA.split()
for s in arrayA:
    print(s)

arrayB = stringA.split('Ba')
for s in arrayB:
    print(s)

arrayC = stringA.split([' ', 'B'])
for s in arrayC:
    print(s)