xWin = False
oWin = False

# boards = [1,2,3,4,5,6,7,8,9]
# TODO: Refactor dictionary variable
board = {'1':' ', '2':' ', '3':' ', '4':' ', '5':' ', '6':' ', '7':' ', '8':' ', '9':' '}

# print board in console
def printBoard():
    print(board['7']+"|"+board['8']+"|"+board['9'])
    print("-+-+-")
    print(board['4']+"|"+board['5']+"|"+board['6'])
    print("-+-+-")
    print(board['1']+"|"+board['2']+"|"+board['3'])


# TODO: Refactor
# Determine who player win by checking same value in a row.
def whoWin():
    if board['1'] == board['2'] == board['3']:
        return board['1']
    if board['4'] == board['5'] == board['6']:
        return board['4']
    if board['7'] == board['8'] == board['9']:
        return board['7']

    if board['1'] == board['4'] == board['7']:
        return board['1']
    if board['2'] == board['5'] == board['8']:
        return board['2']
    if board['3'] == board['6'] == board['9']:
        return board['3']
    
    if board['1'] == board['5'] == board['9']:
        return board['1']
    if board['3'] == board['5'] == board['7']:
        return board['3']

    return False

# Prompt maximum 9 attempts for two players
for step in range(9):
    if step % 2 == 0:
        x = input("It's your turn, X. Move to which place?")
        board[x] = 'X'
    else:
        o = input("It's your turn, O. Move to which place?")
        board[o] = 'O'
    printBoard()

    # if return not false then must either value 'X', or 'O'
    who = whoWin()
    if who != False and who != ' ':
        print("Congratulation player " + who+", you win the game!")
        break
