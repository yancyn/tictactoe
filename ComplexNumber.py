class ComplexNumber:

    real = 0
    imag = 0

    # Default constructor for ComplexNumbver class
    # where r and i is a float number.
    def __init__(self, r=0.0, i=0.0):
        self.real = r
        self.imag = i

    def __str__(self):
        if self.imag < 0:
            return f'{self.real}{self.imag}i'
        else:
            return f'{self.real}+{self.imag}i'

    def getData(self):
        # print(f'{self.real}+{self.imag}')
        return f'{self.real}+{self.imag}'


# num1 = ComplexNumber(2,3)
# num1.getData()