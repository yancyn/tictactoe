listA = [1,2,3]
for i in listA:
    print(i)

listB = ['A string', 23, 1/3, 100.232, '∅']
for s in listB:
    print(s)
print(len(listB))
print("Length of list B: " + "5")
print("Length of list B: " + str(len(listB)))

listB += ['new item']
for s in listB:
    print(s)

# listB.append('append item').append('second item') equal to .extend()
listB.append('append item')
for s in listB:
    print(s)

#  remove last item by default if not specified
listB.pop()
# equavalent to
# lastIndex = len(listB) - 1
# listB.pop(lastIndex)
for s in listB:
    print(s)
    print('---')

listC = ['a', 'e', 'x', 'b', 'c']
for s in listC:
    print(s)

print()
listC.reverse()
for s in listC:
    print(s)

print()
listC.sort()
for s in listC:
    print(s)

print()
for s in listC[1:3]:
    print(s)