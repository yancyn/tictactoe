# Tic Tac Toe
A Python hand's on project to write a simple tic tac toe game.

## TODO
1. [ ] Looking python source code for Raspberry Pi treat it as hand's on.

## Requirements
- Python3 (python 2 or earlier obsolete since Apr 2020)
- PIP - Package Installer
- Pandas - Data manipulation uses

## Setup
```bash
$ sudo apt install python3
$ sudo apt install python3-pip
$ pip3 install pandas
```

## Install Python v3.8 on Ubuntu
```bash
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo apt-repository ppa:deadsnakes/ppa
$ sudo apt install pytyhon3.8 pythong3-tk
```
Now you can compile python script with command `python3.8 main.py` see https://python.tutorials24x7.com/blog/how-to-install-python-3-8-on-ubuntu-18-04-lts

## Tic Tac Toe 1 (Console)
```bash
$ python3.8 tictactoe1.py
```

![console.png](console.png)

![design1.PNG](design1.PNG)

## References
1. https://www.python.org/
2. https://pypi.org/
3. https://pandas.pydata.org/
4. https://docs.python.org/3/library/
5. Online Python complier https://repl.it/languages/python3
6. https://docs.python.org/3/library/