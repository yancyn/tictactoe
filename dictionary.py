# value can be any data type i.e. string, float, or even an array
dictionary1 = {'A': 'Apple', 'B':['Banana', 'Bee'], '甲': 'unicode', 'A': 'Anna and the King', 1: 'One'}
for s in dictionary1:
    print(s)
print(dictionary1['A'])
print(dictionary1[1])

print()
for s in dictionary1['B']:
    print(s)
print(dictionary1['甲'])

# key also can be other data type like integer
print()
dictionary2 = {1: 'one', 2: 'two'}
for s in dictionary2:
    print(s)

print()
dictionary3 = {}
dictionary3['A'] = 'Apple'
dictionary3['B'] = 'Banana'
dictionary3['C'] = 'Ciku'
for s in dictionary3:
    print(dictionary3[s])

print()
print(dictionary3['A'])
# we cannot specific a range or index in dictionary
#for s in dictionary3['A':'B']:
#    print(s)