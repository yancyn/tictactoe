from tkinter import *

def display():
    """
    Display message after press the button.
    """

    lbl1.configure(
        text="My anaconda don't... \n want none unless you got buns, hun")

# initial window
window = Tk()
window.title("Welcome to Python 301")
window.geometry('350x200')

# Add label
lbl1 = Label(
    window,
    text="My anaconda don't...",
    # font=("Arial Bold", 10),
    foreground="black",
    background="#c20430",
    width=35,
    height=2)
lbl1.grid(column=0, row=0)

# Add buton
btn = Button(
    window,
    text="Click Me",
    bg="orange",
    fg="red",
    command=display)
btn.grid(column=0, row=1)

# start window
window.mainloop()