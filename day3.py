import math

number = 123
print(type(number))

complex = 1+1j
print(math.sqrt(complex.real * complex.real + complex.imag * complex.imag))

string = "Test"
listOfItem = [1,2,3]
dictOfItem = {1: "one"}
tupleOfItem = (1, "one")

# How to call function only
import day1
day1.myfunc()

# How to import a module and call a class
from ComplexNumber import *
complexZero = ComplexNumber()
print(type(complexZero.imag))

num2 = ComplexNumber(1,2)
print(num2.getData())

num3 = ComplexNumber(1,-3)
print(str(num3))
print("Imaginary: " + str(num3.imag))