from tkinter import *

window = Tk()
window.title("Welcome to Python 301")

#label = Label(window, text="My anaconda don't...")
# label.grid(column=0, row=0)

label = Label(
    text="Hello, Tkinter",
    foreground="white",
    background="#C20430",
    width=10,
    height=10,
    font=("Courier", 12)
)
label.pack()

# start the window
window.mainloop()