firstValue = input("Enter first value: ")
secondValue = input("Enter second value: ")
print("You have entered first value: ", firstValue)
print("You have entered second value: ", secondValue)

#  by default compiler assume all variables are string if not defined
answer = firstValue + secondValue
print("Answer: ", answer)

firstValue = int(input("Enter first value: "))
secondValue = int(input("Enter second value: "))
print("You have entered first value: ", firstValue)
print("You have entered second value: ", secondValue)
answer = firstValue + secondValue
print("Answer: ", answer)