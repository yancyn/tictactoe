class Animal:
    """
    A class used to represent an Animal.

    Attributes
    ----------
    voice : str
        A formatted string to print out what the animal says.
    name : str
        A name of the animal.
    numOfLegs : int
        The number of legs the animal has (default 4).

    Methods
    -------
    says(sound=None)
        Prints the animals name and what sound it makes.
    """

    voice = "A {name} says {sound}"

    def __init__(self, name, sound, numOfLegs = 4):
        """
        Parameters
        ----------
        name : str
            The name of the animal.
        sound : str
            The sound the animal makes.
        numOfLegs : int, optional
            The number of legs the animal (default 4).
        """

        self.name = name
        self.sound = sound
        self.numOfLegs = numOfLegs
    
    def __str__(self):
        return self.name

    def says(self, sound = None):
        """
        Prints what the animal name and what the sound it makes.

        If the argument `sound` isn't passed in, the default Animal sound is used.

        Parameters
        ----------
        sound : str, optional
            The sound the animal makes (default is None).
        
        Raises
        ------
        NotImplementedError
            If no sound is set for this animal or passed in as a parameter.
        """

        if self.sound is None and sound is None:
            raise NotImplementedError("Silent animals are not supported!")

        outSound = self.sound if sound is None else sound
        print(self.voice.format(name=self, sound=outSound))
